# Prueba Técnica - Aplicación de Eventos

## Descripción del Proyecto

El proyecto actual es una aplicación que sirve para gestionar Eventos. Asimismo, está diseñada para trabajar con grandes cantidades de datos, proporcionando funcionalidades de búsqueda eficientes. 
La aplicación se centra en tres tipos de búsquedas:
- Buscar eventos que contengan un nombre de fuente.
- Buscar eventos entre fechas (fecha inicio, fecha fin).
- Buscar eventos dentro de un rango de valores (valor mínimo, valor máximo).

Cada evento contiene un objeto fuente dentro de su respuesta.

## Requisitos Técnicos

El **lenguaje** de programación es Java y la versión elegida es la última LTS disponible: 21. 

Además, se ha elegido Maven como herramienta para la gestión y construcción del mismo.

Se han elegido una serie de **dependencias** que se consideran imprescindibles. Entre las cuales se podrían destacar 
- `Lombok`: librería para eliminar código repetitivo mediante el uso de anotaciones. Simplifica el desarrollo de clases, eliminando la necesidad de escribir métodos como `getters`, `setters`, etc.
- `Junit`: librería para escribir pruebas unitarias.

Todos los datos se almacenan en memoria y no se trabaja con ningún tipo de base de datos.

Se ha tratado de **paralelizar** todas las tareas posibles para agilizar la ejecución del proyecto. Para ello, se ha hecho uso del API Stream de Java.

Es importante destacar que se han empleado distintos patrones de diseño para la consecución de los objetivos:
- Data transfer object (DTO): Un DTO es un objeto simple que se utiliza para transportar datos entre capas de una aplicación. Suele ser ligero y no contiene lógica de negocio. Su objetivo principal es facilitar el intercambio de datos entre diferentes partes de la aplicación sin exponer la implementación interna de las clases.
- Singleton: Un patrón Singleton es un patrón de diseño que garantiza que solo exista una instancia de una clase en toda la aplicación. Esto se logra mediante la creación de un método estático que devuelve la instancia única de la clase. El patrón Singleton se utiliza cuando se necesita una instancia global de una clase que sea accesible desde cualquier parte de la aplicación.
- Utility: Un patrón Utility es un patrón de diseño que proporciona métodos estáticos de utilidad para diferentes tareas. Estos métodos suelen ser simples y no requieren acceso al estado de una clase. El patrón Utility se utiliza para organizar el código de utilidad en un solo lugar y facilitar su reutilización.
- Service: Un patrón Service es un patrón de diseño que encapsula la lógica de negocio de una aplicación. Los servicios suelen proporcionar una interfaz definida que expone métodos para realizar operaciones específicas. El patrón Service se utiliza para separar la lógica de negocio de la capa de presentación y facilitar su reutilización.
- Facade: El patrón Facade es un patrón de diseño estructural que proporciona una interfaz simple y unificada para acceder a un subsistema complejo, ocultando su complejidad interna. El patrón Facade actúa como una capa intermedia entre los clientes y el subsistema, simplificando la interacción y reduciendo el acoplamiento entre ellos.
- Repository: Un patrón Repository es un patrón de diseño que proporciona una abstracción para acceder a datos de una fuente externa, como una base de datos. Los repositorios suelen proporcionar métodos para realizar operaciones CRUD (Create, Read, Update, Delete) sobre los datos. El patrón Repository se utiliza para separar la capa de acceso a datos de la lógica de negocio.

## Estructura del Proyecto

```plaintext
├── src
│   ├── main
│   │   ├── java
│   │   │   └── es
│   │   │       └── joninx
│   │   │           └── ariadna
│   │   │               ├── App.java
│   │   │               ├── dto
│   │   │               │   ├── EventDto.java
│   │   │               │   └── SourceDto.java
│   │   │               ├── factory
│   │   │               │   ├── EventFactory.java
│   │   │               │   └── SourceFactory.java
│   │   │               ├── io
│   │   │               │   └── FileLoader.java
│   │   │               ├── model
│   │   │               │   ├── Event.java
│   │   │               │   └── Source.java
│   │   │               ├── repository
│   │   │               │   ├── DataRepository.java
│   │   │               │   ├── EventsRepository.java
│   │   │               │   └── SourcesRepository.java
│   │   │               ├── service
│   │   │               │   ├── DatabaseService.java
│   │   │               │   └── Facade.java
│   │   │               └── util
│   │   │                   ├── DateTimeUtil.java
│   │   │                   └── NumberUtil.java
│   │   ├── resources
│   │   ├── sources.csv
│   │   ├── events_part_1.csv
│   │   ├── events_part_2.csv
│   │   ├── events_part_3.csv
│   │   ├── events_part_4.csv
│   │   ├── events_part_5.csv
│   │   ├── events_part_6.csv
│   ├── test
│   │   ├── java
│   │   │   └── es
│   │   │       └── joninx
│   │   │           └── ariadna
│   │   │               └── UnitTest.java
├── .gitignore
├── pom.xml
└── README.md: fichero de instrucciones
```

## Notas de Desarrollo

Para el desarrollo, se ha decidido apoyarse en la documentación oficial de Java, tal y como se suele hacer habitualmente.

Por otro lado, dados los tiempos que corren resultaría extraño no incluir el uso del LLM ChatGPT para las consultas rápidas.

Los siguientes aspectos se han tenido en cuenta a la hora de desarrollar este proyecto:
- Eficiencia: Las búsquedas por timestamp y rango de valor se han optimizado para ser lo más eficientes posible dentro del contexto de una prueba técnica.
- Carga Paralela: Se utiliza una carga paralela para cargar los archivos de eventos de manera concurrente, mejorando los tiempos de carga.
- Patrones de diseño: se considera imprescindible apoyarse en patrones de diseño para el desarrollo de un producto más robusto y con una calidad considerable. 
- Maven: imprescindible en los proyectos Java. Ayuda en la gestión de dependencias y la ejecución del proyecto.

Cabe destacar que la mayoría de las clases están documentadas mediante Javadocs.

Como entorno de desarrollo, se ha decidido hacer uso del software de Jetbrains IntelliJ IDEA. El autor del proyecto tiene licencia original, puesto que ofrecen a las personas estudiantes una licencia gratuita.

## Archivos de Datos

Para la creación de los archivos de datos, se ha empleado ChatGPT, de tal manera que ha resultado más llevadera la tarea de creación de los propios datos.

## Ejecución

Los siguientes requisitos debes cumplir:
- Git: para clonar el repositorio en local.
- Java: JDK 21 compatible instalado y funcionando desde línea de comandos.
- Maven: no se ha facilitado ningún Maven embebido, por lo que necesitarás tenerlo instalado.

Debes seguir los siguientes pasos para poder ejecutar el proyecto:

1. Clonar el repositorio

Desde línea de comandos, nos clonamos el repositorio en local.

```powershell
git clone https://gitlab.com/russellhoff/ariadna.git
cd ariadna/
```

3. Compilar el proyecto y ejecutar los tests

Mediante maven, limpiamos el directorio `target` (por si estuviera con datos) y ejecutamos el _goal_ **install** (compila, ejecuta el goal test y empaqueta el proyecto).

```powershell
mvn clean install
```

Ejemplo de ejecución:

```powershell
PS C:\workspaces\workspace-java\ariadna-tech> mvn clean install
[INFO] Scanning for projects...
[INFO]
[INFO] -------------------< es.joninx.ariadna:ariadna-tech >-------------------
[INFO] Building ariadna-tech 2024.6
[INFO]   from pom.xml
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- clean:3.3.2:clean (default-clean) @ ariadna-tech ---
[INFO] Deleting C:\workspaces\workspace-java\ariadna-tech\target
[INFO]
[INFO] --- resources:3.3.1:resources (default-resources) @ ariadna-tech ---
[INFO] Copying 7 resources from src\main\resources to target\classes
[INFO]
[INFO] --- compiler:3.11.0:compile (default-compile) @ ariadna-tech ---
[INFO] Changes detected - recompiling the module! :source
[INFO] Compiling 15 source files with javac [debug target 21] to target\classes
[INFO] Annotation processing is enabled because one or more processors were found
  on the class path. A future release of javac may disable annotation processing
  unless at least one processor is specified by name (-processor), or a search
  path is specified (--processor-path, --processor-module-path), or annotation
  processing is enabled explicitly (-proc:only, -proc:full).
  Use -Xlint:-options to suppress this message.
  Use -proc:none to disable annotation processing.
[INFO]
[INFO] --- resources:3.3.1:testResources (default-testResources) @ ariadna-tech ---
[INFO] Copying 0 resource from src\test\resources to target\test-classes
[INFO]
[INFO] --- compiler:3.11.0:testCompile (default-testCompile) @ ariadna-tech ---
[INFO] Changes detected - recompiling the module! :dependency
[INFO] Compiling 2 source files with javac [debug target 21] to target\test-classes
[INFO] Annotation processing is enabled because one or more processors were found
  on the class path. A future release of javac may disable annotation processing
  unless at least one processor is specified by name (-processor), or a search
  path is specified (--processor-path, --processor-module-path), or annotation
  processing is enabled explicitly (-proc:only, -proc:full).
  Use -Xlint:-options to suppress this message.
  Use -proc:none to disable annotation processing.
[INFO]
[INFO] --- surefire:3.2.2:test (default-test) @ ariadna-tech ---
[INFO] Using auto detected provider org.apache.maven.surefire.junitplatform.JUnitPlatformProvider
[INFO]
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running es.joninx.ariadna.MainTest
[*]> Setting up tests...
 > Loading initial data...
                 + Processing 'events_part_2.csv' file.
                 + Processing 'events_part_5.csv' file.
                 + Processing 'events_part_1.csv' file.
                 + Processing 'events_part_4.csv' file.
                 + Processing 'events_part_3.csv' file.
                 + Processing 'events_part_6.csv' file.
 > Initial data loaded.
[3]> Find events by source name test
[3]> ===============================
[1]> Data loaded test
[1]> ================
[2]> Find event by id test
[2]> =====================
[5]> Find events by value range test
[5]> ===============================
[4]> Find events by date range test
[4]> ==============================
[*]> Ending up tests...
[INFO] Tests run: 5, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.175 s -- in es.joninx.ariadna.MainTest
[INFO] Running es.joninx.ariadna.UnitTest
[INFO] Tests run: 8, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.020 s -- in es.joninx.ariadna.UnitTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 13, Failures: 0, Errors: 0, Skipped: 0
[INFO]
[INFO]
[INFO] --- jar:3.3.0:jar (default-jar) @ ariadna-tech ---
[INFO] Building jar: C:\workspaces\workspace-java\ariadna-tech\target\ariadna-tech.jar
[INFO]
[INFO] --- install:3.1.1:install (default-install) @ ariadna-tech ---
[INFO] Installing C:\workspaces\workspace-java\ariadna-tech\pom.xml to C:\Users\Jon I\.m2\repository\es\joninx\ariadna\ariadna-tech\2024.6\ariadna-tech-2024.6.pom
[INFO] Installing C:\workspaces\workspace-java\ariadna-tech\target\ariadna-tech.jar to C:\Users\Jon I\.m2\repository\es\joninx\ariadna\ariadna-tech\2024.6\ariadna-tech-2024.6.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  4.197 s
[INFO] Finished at: 2024-06-16T19:49:57+02:00
[INFO] ------------------------------------------------------------------------
```

Si no queremos ejecutar los tests le tenemos que pasar el flag `-DskipTests`.

Ejemplo de ejecución:

```powershell
PS C:\workspaces\workspace-java\ariadna-tech> mvn clean install -DskipTests
[INFO] Scanning for projects...
[INFO]
[INFO] -------------------< es.joninx.ariadna:ariadna-tech >-------------------
[INFO] Building ariadna-tech 2024.6
[INFO]   from pom.xml
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- clean:3.3.2:clean (default-clean) @ ariadna-tech ---
[INFO] Deleting C:\workspaces\workspace-java\ariadna-tech\target
[INFO]
[INFO] --- resources:3.3.1:resources (default-resources) @ ariadna-tech ---
[INFO] Copying 7 resources from src\main\resources to target\classes
[INFO]
[INFO] --- compiler:3.11.0:compile (default-compile) @ ariadna-tech ---
[INFO] Changes detected - recompiling the module! :source
[INFO] Compiling 15 source files with javac [debug target 21] to target\classes
[INFO] Annotation processing is enabled because one or more processors were found
  on the class path. A future release of javac may disable annotation processing
  unless at least one processor is specified by name (-processor), or a search
  path is specified (--processor-path, --processor-module-path), or annotation
  processing is enabled explicitly (-proc:only, -proc:full).
  Use -Xlint:-options to suppress this message.
  Use -proc:none to disable annotation processing.
[INFO]
[INFO] --- resources:3.3.1:testResources (default-testResources) @ ariadna-tech ---
[INFO] Copying 0 resource from src\test\resources to target\test-classes
[INFO]
[INFO] --- compiler:3.11.0:testCompile (default-testCompile) @ ariadna-tech ---
[INFO] Changes detected - recompiling the module! :dependency
[INFO] Compiling 2 source files with javac [debug target 21] to target\test-classes
[INFO] Annotation processing is enabled because one or more processors were found
  on the class path. A future release of javac may disable annotation processing
  unless at least one processor is specified by name (-processor), or a search
  path is specified (--processor-path, --processor-module-path), or annotation
  processing is enabled explicitly (-proc:only, -proc:full).
  Use -Xlint:-options to suppress this message.
  Use -proc:none to disable annotation processing.
[INFO]
[INFO] --- surefire:3.2.2:test (default-test) @ ariadna-tech ---
[INFO] Tests are skipped.
[INFO]
[INFO] --- jar:3.3.0:jar (default-jar) @ ariadna-tech ---
[INFO] Building jar: C:\workspaces\workspace-java\ariadna-tech\target\ariadna-tech.jar
[INFO]
[INFO] --- install:3.1.1:install (default-install) @ ariadna-tech ---
[INFO] Installing C:\workspaces\workspace-java\ariadna-tech\pom.xml to C:\Users\Jon I\.m2\repository\es\joninx\ariadna\ariadna-tech\2024.6\ariadna-tech-2024.6.pom
[INFO] Installing C:\workspaces\workspace-java\ariadna-tech\target\ariadna-tech.jar to C:\Users\Jon I\.m2\repository\es\joninx\ariadna\ariadna-tech\2024.6\ariadna-tech-2024.6.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.420 s
[INFO] Finished at: 2024-06-16T19:50:50+02:00
[INFO] ------------------------------------------------------------------------
```

4. Ejecutar la clase main del proyecto

Simplemente hacemos:

```powershell
java -cp target/classes es.joninx.ariadna.App
```

Ejemplo de ejecución:

```powershell
PS C:\workspaces\workspace-java\ariadna-tech> java -cp target/classes es.joninx.ariadna.App
Starting...
 > Loading initial data...
                 + Processing 'events_part_3.csv' file.
                 + Processing 'events_part_4.csv' file.
                 + Processing 'events_part_6.csv' file.
                 + Processing 'events_part_2.csv' file.
                 + Processing 'events_part_1.csv' file.
                 + Processing 'events_part_5.csv' file.
 > Initial data loaded.
Started.
```

5. Ejecutar los tests

Ejecutamos el goal test de maven.

```powershell
mvn test
```

Ejemplo de ejecución:

```powershell
PS C:\workspaces\workspace-java\ariadna-tech> mvn test
[INFO] Scanning for projects...
[INFO]
[INFO] -------------------< es.joninx.ariadna:ariadna-tech >-------------------
[INFO] Building ariadna-tech 2024.6
[INFO]   from pom.xml
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- resources:3.3.1:resources (default-resources) @ ariadna-tech ---
[INFO] Copying 7 resources from src\main\resources to target\classes
[INFO]
[INFO] --- compiler:3.11.0:compile (default-compile) @ ariadna-tech ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] --- resources:3.3.1:testResources (default-testResources) @ ariadna-tech ---
[INFO] Copying 0 resource from src\test\resources to target\test-classes
[INFO]
[INFO] --- compiler:3.11.0:testCompile (default-testCompile) @ ariadna-tech ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] --- surefire:3.2.2:test (default-test) @ ariadna-tech ---
[INFO] Using auto detected provider org.apache.maven.surefire.junitplatform.JUnitPlatformProvider
[INFO]
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running es.joninx.ariadna.MainTest
[*]> Setting up tests...
 > Loading initial data...
                 + Processing 'events_part_1.csv' file.
                 + Processing 'events_part_6.csv' file.
                 + Processing 'events_part_2.csv' file.
                 + Processing 'events_part_3.csv' file.
                 + Processing 'events_part_4.csv' file.
                 + Processing 'events_part_5.csv' file.
 > Initial data loaded.
[3]> Find events by source name test
[3]> ===============================
[1]> Data loaded test
[1]> ================
[2]> Find event by id test
[2]> =====================
[5]> Find events by value range test
[5]> ===============================
[4]> Find events by date range test
[4]> ==============================
[*]> Ending up tests...
[INFO] Tests run: 5, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.146 s -- in es.joninx.ariadna.MainTest
[INFO] Running es.joninx.ariadna.UnitTest
[INFO] Tests run: 8, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.015 s -- in es.joninx.ariadna.UnitTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 13, Failures: 0, Errors: 0, Skipped: 0
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.506 s
[INFO] Finished at: 2024-06-16T19:51:41+02:00
[INFO] ------------------------------------------------------------------------
```