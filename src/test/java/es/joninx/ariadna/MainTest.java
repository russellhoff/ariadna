package es.joninx.ariadna;

import es.joninx.ariadna.model.Event;
import es.joninx.ariadna.service.Facade;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MainTest {

    static Facade fac = Facade.getInstance();

    @BeforeAll
    static void setup() throws URISyntaxException, IOException {
        System.out.println("[*]> Setting up tests...");
        fac.init();
    }

    @AfterAll
    static void teardown(){
        System.out.println("[*]> Ending up tests...");
    }

    /**
     * Method to test whether the data has been successfully loaded.
     */
    @Test
    @Order(1)
    void dataLoadedTest() {
        System.out.println("[1]> Data loaded test");
        assertTrue(fac.eventsExist());
        assertTrue(fac.sourcesExist());
        System.out.println("[1]> ================");
    }

    /**
     * Test to find events by ID.
     * We are going to test two use cases:
     * <li>Case with no data: Id 0</li>
     * <li>Case with data: Id 500</li>
     */
    @Test
    @Order(2)
    void findEventById(){
        System.out.println("[2]> Find event by id test");
        Integer
            inexistentId = 0,
            actualId = 500;

        Optional<Event> inexistent = fac.findEventById(inexistentId);
        assertTrue(inexistent.isEmpty());

        Optional<Event> actual = fac.findEventById(actualId);
        assertTrue(actual.isPresent());
        System.out.println("[2]> =====================");
    }

    /**
     * Test to find events by source name.
     * We are going to test two use cases:
     * <li>Case with no data: "Made up name"</li>
     * <li>Case with data: "Power Surge"</li>
     */
    @Test
    @Order(3)
    void findEventsBySourceNameTest(){
        System.out.println("[3]> Find events by source name test");
        String
            inexistent = "Made up name",
            actual = "Power Surge";

        List<Event> inexistentList = fac.findEventsBySourceName(inexistent);
        assertTrue(inexistentList.isEmpty());

        List<Event> actualList = fac.findEventsBySourceName(actual);
        assertFalse(actualList.isEmpty());
        System.out.println("[3]> ===============================");
    }

    /**
     * Test to find events by date range.
     * We are going to test two use cases:
     * <li>Range with no data: 2010-01-01 .. 2010-12-31</li>
     * <li>Range with data: 2024-05-01 .. 2024-05-31</li>
     */
    @Test
    @Order(4)
    void findEventsByDateTest(){
        System.out.println("[4]> Find events by date range test");
        LocalDate
            inexInit = LocalDate.of(2010, 1, 1),
            inexEnd = LocalDate.of(2010, 12, 31),
            actualInit = LocalDate.of(2024, 5, 1),
            actualEnd = LocalDate.of(2024, 5, 31);

        List<Event> inexistentList = fac.findEventsByDate(inexInit, inexEnd);
        assertTrue(inexistentList.isEmpty());

        List<Event> actualList = fac.findEventsByDate(actualInit, actualEnd);
        assertFalse(actualList.isEmpty());
        System.out.println("[4]> ==============================");
    }

    /**
     * Test to find events by value.
     * We are going to test two use cases:
     * <li>Range with no data: -50 .. -10</li>
     * <li>Range with data: 30 .. 50</li>
     */
    @Test
    @Order(5)
    void findEventsByValueTest(){
        System.out.println("[5]> Find events by value range test");
        Double
            inexMin = -50d,
            inexMax = -10d,
            actualMin = 30d,
            actualMax = 50d;

        List<Event> inexistentList = fac.findEventsByValue(inexMin, inexMax);
        assertTrue(inexistentList.isEmpty());

        List<Event> actualList = fac.findEventsByValue(actualMin, actualMax);
        assertFalse(actualList.isEmpty());
        System.out.println("[5]> ===============================");
    }

}
