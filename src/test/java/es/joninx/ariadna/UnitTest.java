package es.joninx.ariadna;

import es.joninx.ariadna.factory.EventFactory;
import es.joninx.ariadna.factory.SourceFactory;
import es.joninx.ariadna.model.Event;
import es.joninx.ariadna.model.Source;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for model classes.
 */
public class UnitTest {

    public static Source s1, s2, s3, s4;
    public static Event e1, e2, e3, e4, e5, e6, e7, e8;

    private static Source getRandomSource(){
        // Crear un array con los elementos
        Source[] elements = new Source[]{s1, s2, s3, s4};

        // Generar un índice aleatorio entre 0 y 3
        int randomIndex = ThreadLocalRandom.current().nextInt(0, elements.length);

        // Devolver el elemento en el índice aleatorio
        return elements[randomIndex];
    }

    private static LocalDateTime getRandomTimestamp(){
        LocalDateTime start = LocalDateTime.of(2024, 1, 1, 0, 0);
        LocalDateTime end = LocalDateTime.of(2024, 12, 31, 23, 59);

        long startEpochSecond = start.toEpochSecond(ZoneOffset.UTC);
        long endEpochSecond = end.toEpochSecond(ZoneOffset.UTC);

        // Generate random epoch second between start and end
        long randomEpochSecond = ThreadLocalRandom.current().nextLong(startEpochSecond, endEpochSecond);

        // Convert random epoch second back to LocalDateTime
        return LocalDateTime.ofEpochSecond(randomEpochSecond, 0, ZoneOffset.UTC);
    }

    public static double getRandomDoubleBetween() {
        double start = 0.0;
        double end = 200.0;
        // Generar un número aleatorio entre startInclusive y endExclusive
        return ThreadLocalRandom.current().nextDouble(start, end);
    }

    @BeforeAll
    static void setup(){
        s1 = SourceFactory.build(1, "Transformer Overload");
        s2 = SourceFactory.build(2, "Power Surge");
        s3 = SourceFactory.build(3, "Short Circuit");
        s4 = SourceFactory.build(4, "Ground Fault");

        e1 = EventFactory.build(1, getRandomSource(), getRandomTimestamp(), getRandomDoubleBetween());
        e2 = EventFactory.build(2, getRandomSource(), getRandomTimestamp(), getRandomDoubleBetween());
        e3 = EventFactory.build(3, getRandomSource(), getRandomTimestamp(), getRandomDoubleBetween());
        e4 = EventFactory.build(4, getRandomSource(), getRandomTimestamp(), getRandomDoubleBetween());
        e5 = EventFactory.build(5, getRandomSource(), getRandomTimestamp(), getRandomDoubleBetween());
        e6 = EventFactory.build(6, getRandomSource(), getRandomTimestamp(), getRandomDoubleBetween());
        e7 = EventFactory.build(7, getRandomSource(), getRandomTimestamp(), getRandomDoubleBetween());
        e8 = EventFactory.build(8, getRandomSource(), getRandomTimestamp(), getRandomDoubleBetween());
    }

    @Test
    @Order(1)
    void testSourcesNullability() {
        assertNotNull(s1);
        assertNotNull(s2);
        assertNotNull(s3);
        assertNotNull(s4);
    }

    @Test
    @Order(2)
    void testSourcesId(){
        assertEquals(s1.getId(), 1);
        assertEquals(s2.getId(), 2);
        assertEquals(s3.getId(), 3);
        assertEquals(s4.getId(), 4);
    }

    @Test
    @Order(3)
    void testSourcesName(){
        assertEquals(s1.getName(), "Transformer Overload");
        assertNotEquals(s1.getName(), "Haha!");
        assertEquals(s2.getName(), "Power Surge");
        assertNotEquals(s2.getName(), "Distinct");
        assertEquals(s3.getName(), "Short Circuit");
        assertNotEquals(s3.getName(), "Distinct circuit");
        assertEquals(s4.getName(), "Ground Fault");
        assertNotEquals(s4.getName(), "Air Fault");
    }

    @Test
    @Order(4)
    void testEventsNullability(){
        assertNotNull(e1);
        assertNotNull(e2);
        assertNotNull(e3);
        assertNotNull(e4);
        assertNotNull(e5);
        assertNotNull(e6);
        assertNotNull(e7);
        assertNotNull(e8);
    }

    @Test
    @Order(5)
    void testEventsId(){
        assertEquals(e1.getId(), 1);
        assertEquals(e2.getId(), 2);
        assertEquals(e3.getId(), 3);
        assertEquals(e4.getId(), 4);
        assertEquals(e5.getId(), 5);
        assertEquals(e6.getId(), 6);
        assertEquals(e7.getId(), 7);
        assertEquals(e8.getId(), 8);
    }

    @Test
    @Order(6)
    void testEventsTimestamp(){
        LocalDateTime start = LocalDateTime.of(2023, 12, 31, 23, 59, 59);
        assertTrue(e1.getTimestamp().isAfter(start));
        assertTrue(e2.getTimestamp().isAfter(start));
        assertTrue(e3.getTimestamp().isAfter(start));
        assertTrue(e4.getTimestamp().isAfter(start));
        assertTrue(e5.getTimestamp().isAfter(start));
        assertTrue(e6.getTimestamp().isAfter(start));
        assertTrue(e7.getTimestamp().isAfter(start));
        assertTrue(e8.getTimestamp().isAfter(start));
    }

    @Test
    @Order(7)
    void testEventsSource(){
        assertNotNull(e1.getSource());
        assertNotNull(e2.getSource());
        assertNotNull(e3.getSource());
        assertNotNull(e4.getSource());
        assertNotNull(e5.getSource());
        assertNotNull(e6.getSource());
        assertNotNull(e7.getSource());
        assertNotNull(e8.getSource());
    }

    @Test
    @Order(8)
    void testEventsValue(){
        assertTrue(e1.getValue()>=0.0 && e1.getValue()<=200.0);
        assertTrue(e2.getValue()>=0.0 && e2.getValue()<=200.0);
        assertTrue(e3.getValue()>=0.0 && e3.getValue()<=200.0);
        assertTrue(e4.getValue()>=0.0 && e4.getValue()<=200.0);
        assertTrue(e5.getValue()>=0.0 && e5.getValue()<=200.0);
        assertTrue(e6.getValue()>=0.0 && e6.getValue()<=200.0);
        assertTrue(e7.getValue()>=0.0 && e7.getValue()<=200.0);
        assertTrue(e8.getValue()>=0.0 && e8.getValue()<=200.0);
    }

}
