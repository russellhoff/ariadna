package es.joninx.ariadna.factory;

import es.joninx.ariadna.dto.SourceDto;
import es.joninx.ariadna.model.Source;

/**
 * Source factory.
 */
public class SourceFactory {

    /**
     * Build Source by passing data as parameters.
     * @param id Source's id.
     * @param name Source's name.
     * @return Created Source.
     */
    public static Source build(
        Integer id,
        String name
    ){
        return Source.builder()
            .id(id)
            .name(name)
            .build();
    }

    /**
     * Create a Source based on its DTO class.
     * @param dto Source's DTO object.
     * @return Created Source.
     */
    public static Source fromDto(SourceDto dto){
        return Source.builder()
            .id(dto.getId())
            .name(dto.getName())
            .build();
    }

}
