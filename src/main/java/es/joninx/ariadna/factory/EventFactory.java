package es.joninx.ariadna.factory;

import es.joninx.ariadna.model.Event;
import es.joninx.ariadna.dto.EventDto;
import es.joninx.ariadna.model.Source;
import es.joninx.ariadna.util.DateTimeUtil;

import java.time.LocalDateTime;

/**
 * Event factory.
 */
public class EventFactory {

    /**
     * Build Event by passing data as parameters.
     * @param id Event's id number.
     * @param source Event's precreated source.
     * @param timestamp Event's timestamp.
     * @param value Event's value.
     * @return Created Event.
     */
    public static Event build(
        Integer id,
        Source source,
        LocalDateTime timestamp,
        Double value
    ){
        return Event.builder()
            .id(id)
            .source(source)
            .timestamp(timestamp)
            .value(value)
            .build();
    }

    /**
     * Create an Event based on its DTO class.
     * @param dto Event's DTO object.
     * @param source The precreated source.
     * @return Created Event.
     */
    public static Event fromDto(
        EventDto dto,
        Source source
    ){
        return Event.builder()
                .id(dto.getId())
                .source(source)
                .timestamp(DateTimeUtil.parse(dto.getTimestamp()))
                .value(dto.getValue())
                .build();
    }

}
