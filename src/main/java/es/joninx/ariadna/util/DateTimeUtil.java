package es.joninx.ariadna.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * DateTime utility class.
 */
public class DateTimeUtil {

    public static final DateTimeFormatter f = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    /**
     * Formats a LocalDateTime object.
     * @param date The LDT class.
     * @return LDT stringified.
     */
    public static String format(LocalDateTime date){
        return date.format(f);
    }

    /**
     * Parses an LDT from a string.
     * @param date The date time in string format.
     * @return The date in LDT format.
     */
    public static LocalDateTime parse(String date){
        return LocalDateTime.parse(date, f);
    }

}
