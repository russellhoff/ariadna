package es.joninx.ariadna.util;

import java.text.DecimalFormat;

/**
 * Number utility class.
 */
public class NumberUtil {

    private static final DecimalFormat df1 = new DecimalFormat("0.0");
    private static final DecimalFormat df2 = new DecimalFormat("0.00");
    private static final DecimalFormat df3 = new DecimalFormat("0.000");

    /**
     * Formats a double to One decimal place.
     * @param d Double value to format
     * @return String value formatted.
     */
    public static String format1(Double d) {
        return df1.format(d);
    }

    /**
     * Formats a double to Two decimal place.
     * @param d Double value to format
     * @return String value formatted.
     */
    public static String format2(Double d) {
        return df2.format(d);
    }

    /**
     * Formats a double to Three decimal place.
     * @param d Double value to format
     * @return String value formatted.
     */
    public static String format3(Double d) {
        return df3.format(d);
    }

}
