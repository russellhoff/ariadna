package es.joninx.ariadna;


import es.joninx.ariadna.service.Facade;

import java.io.IOException;
import java.net.URISyntaxException;

public class App {
    public static void main( String[] args ){
        System.out.println("Starting...");

        Facade fac = Facade.getInstance();
        try {
            fac.init();
            System.out.println("Started.");
        } catch (URISyntaxException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
