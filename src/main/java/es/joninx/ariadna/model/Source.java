package es.joninx.ariadna.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Object representing a Source.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Source {

    /**
     * Source's identifier
     */
    private Integer id;
    /**
     * The name
     */
    private String name;

    /**
     * Tells whether the source has the same name as the one passed by parameter.
     * @param name The name to check for.
     * @return True if it has the same name.
     */
    public Boolean sameName(String name) {
        return this.name.equalsIgnoreCase(name);
    }

    /**
     * To string method.
     * @return Source stringified.
     */
    @Override
    public String toString(){
        return String.format("Source[id='%d', name='%s']", id, name);
    }

}
