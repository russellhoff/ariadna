package es.joninx.ariadna.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import es.joninx.ariadna.util.DateTimeUtil;
import es.joninx.ariadna.util.NumberUtil;
import lombok.*;

/**
 * Class that represent an Event.
 * @author Jon I
 * @since 2024-06-11
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Event {
    /**
     * Identifier.
     */
    private Integer id;
    /**
     * Event's source.
     * Precondition: never null.
     */
    @NonNull
    private Source source;
    /**
     * Timestamp of the event.
     */
    private LocalDateTime timestamp;
    /**
     * Measurement of the event.
     */
    private Double value;

    /**
     * Tells whether the Event has the same Id.
     *
     * @param id The id to check for.
     * @return True if the id is equal. False otherwise.
     */
    public Boolean sameId(Integer id){
        return this.id.equals(id);
    }

    /**
     * Tells whether this Event's source has the same name.
     * @param sourceName The name of the source to check for.
     * @return True
     */
    public Boolean sameSourceName(String sourceName){
        return source.sameName(sourceName);
    }

    /**
     * Tells whether the event is between the date range passed by parameter.
     * @param start The start date. Will be used as LocalDateTime at start of the day.
     * @param end The end date. Will be used as LocalDateTime at the end of the day.
     * @return True if the event is within the range comprised by start and end dates.
     */
    public boolean between(LocalDate start, LocalDate end) {
        return (this.timestamp.isAfter(start.atStartOfDay())
                || this.timestamp.isEqual(start.atStartOfDay())) &&
                this.timestamp.isBefore(end.atTime(LocalTime.MAX));
    }

    /**
     * Tells whether the event is between the range of min and max values.
     * Precondition: min <= max
     * @param min The min value.
     * @param max The max value.
     * @return True if the Event's value is between the range min and max.
     */
    public boolean between(Double min, Double max){
        return this.value>=min &&
                this.value<max;
    }

    /**
     * To string method.
     * @return Event stringified.
     */
    @Override
    public String toString(){
        return String.format("Event[id='%d', source=<%s>, timestamp='%s', value='%s']", id, source, DateTimeUtil.format(timestamp), NumberUtil.format3(value));
    }

}
