package es.joninx.ariadna.io;

import es.joninx.ariadna.dto.EventDto;
import es.joninx.ariadna.dto.SourceDto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to help loading resources located on `src/main/resources` default directory.
 */
public class FileLoader {

    /**
     * Loads a file located on the default resource directory.
     * @param resourceName The resource name.
     * @return Resource's Path object.
     * @throws URISyntaxException Whether an error occur when loading it.
     * @throws IOException Whether an error occur when loading it.
     */
    public static Path loadResource(String resourceName) throws URISyntaxException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL resource = classLoader.getResource(resourceName);
        if (resource == null) {
            throw new IOException("Resource not found: " + resourceName);
        }
        return Paths.get(resource.toURI());
    }

    /**
     * Helps loading a source file into a list of DTOs.
     * @param file The `Path` object full of sources. Precondition: this file contains source data.
     * @return List<SourceDto>
     * @throws IOException Whether an IO error occur.
     */
    public static List<SourceDto> loadSources(Path file) throws IOException {
        List<SourceDto> fuentes = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file.toFile()))) {
            String line;
            boolean init = true;
            while ((line = br.readLine()) != null) {
                if(init){
                    init = false;
                    continue;
                }
                //System.out.println(" > Reading source line: [[" + line + "]]");
                String[] parts = line.split(",");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                fuentes.add(new SourceDto(id, name));
            }
        }
        return fuentes;
    }

    /**
     * Helps loading a events file into a list of DTOs.
     * @param file The `Path` object full of events. Precondition: this file contains event data.
     * @return List<EventDto>
     * @throws IOException Whether an IO error occur.
     */
    public static List<EventDto> loadEvents(Path file) throws IOException {
        List<EventDto> events = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file.toFile()))) {
            String line;
            boolean init = true;
            while ((line = br.readLine()) != null) {
                if(init){
                    init = false;
                    continue;
                }
                //System.out.println(" > Reading event line: [[" + line + "]]");
                String[] parts = line.split(",");
                int id = Integer.parseInt(parts[0]);
                String timestamp = parts[1];
                double value = Double.parseDouble(parts[2]);
                int sourceId = Integer.parseInt(parts[3]);
                events.add(new EventDto(id, sourceId, timestamp, value));
            }
        }
        return events;
    }

}
