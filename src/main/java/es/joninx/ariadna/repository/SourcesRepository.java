package es.joninx.ariadna.repository;

import es.joninx.ariadna.model.Source;

import java.util.*;

/**
 * Repository implementation to store Source elements.
 */
public class SourcesRepository extends DataRepository<Integer, Source> {

    private static SourcesRepository INSTANCE;

    private SourcesRepository() {
        data = Collections.synchronizedMap(new HashMap<>());
    }

    public synchronized static SourcesRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SourcesRepository();
        }
        return INSTANCE;
    }

    @Override
    public Integer count() {
        return data.entrySet().size();
    }

    @Override
    public List<Source> findAll() {
        return data.values().parallelStream().toList();
    }

    @Override
    public Optional<Source> findOne(Integer id) {
        return data.entrySet()
            .parallelStream()
            .filter(elem -> elem.getKey().equals(id))
            .map(Map.Entry::getValue)
            .findFirst();
    }

    @Override
    public void insert(Source elem) {
        data.put(elem.getId(), elem);
    }

    @Override
    public void insertAll(List<Source> elems) {
        elems.parallelStream()
                .forEach(this::insert);
    }

    @Override
    public boolean remove(Source elem) {
        if(data.containsKey(elem.getId())) {
            data.remove(elem.getId());
            return true;
        }
        return false;
    }
}
