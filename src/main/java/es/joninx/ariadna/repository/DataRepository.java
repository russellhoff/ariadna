package es.joninx.ariadna.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Base class to implements Repository pattern.
 * @param <T> Identifier type of E data type objects.
 * @param <E> The data type of the objects that will be stored here.
 */
abstract class DataRepository<T, E> {

    /**
     * Where all the objects will be stored.
     */
    protected Map<T, E> data;

    /**
     * Count the actual data size.
     * @return Amount of objects stored within the `data`.
     */
    abstract Integer count();

    /**
     * Method to find all.
     * @return A list containing all the data.
     */
    abstract List<E> findAll();

    /**
     * Given an id of type T, returns the object.
     * @param id The object's ID.
     * @return An Optional of the object.
     */
    abstract Optional<E> findOne(T id);

    /**
     * Inserts a new element.
     * @param elem The element to insert.
     */
    abstract void insert(E elem);

    /**
     * Inserts a list of elements.
     * @param elems List of elements to insert.
     */
    abstract void insertAll(List<E> elems);

    /**
     * Removes an element.
     * @param elem The element to remove.
     * @return Whether the operation was successful or not.
     */
    abstract boolean remove(E elem);

}
