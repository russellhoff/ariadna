package es.joninx.ariadna.repository;

import es.joninx.ariadna.model.Event;

import java.time.LocalDate;
import java.util.*;

/**
 * Repository implementation to store Events elements.
 */
public class EventsRepository extends DataRepository<Integer, Event> {

    private static EventsRepository INSTANCE;

    private EventsRepository(){
        data = Collections.synchronizedMap(new HashMap<>());
    }

    public synchronized static EventsRepository getInstance(){
        if(INSTANCE == null){
            INSTANCE = new EventsRepository();
        }
        return INSTANCE;
    }

    @Override
    public Integer count() {
        return data.entrySet().size();
    }

    @Override
    public List<Event> findAll() {
        return data.values().parallelStream().toList();
    }

    @Override
    public Optional<Event> findOne(Integer id) {
        return data.entrySet()
                .parallelStream()
                .filter(elem -> elem.getKey().equals(id))
                .map(Map.Entry::getValue)
                .findFirst();
    }

    @Override
    public synchronized void insert(Event elem) {
        data.put(elem.getId(), elem);
    }

    @Override
    public void insertAll(List<Event> elems) {
        elems.parallelStream()
                .forEach(this::insert);
    }

    @Override
    public boolean remove(Event elem) {
        if(data.containsKey(elem.getId())) {
            data.remove(elem.getId());
            return true;
        }
        return false;
    }

    public List<Event> findBySourceName(String sourceName) {
        return data.values()
                .parallelStream()
                .filter(event -> event.sameSourceName(sourceName))
                .toList();
    }

    public List<Event> findByDate(LocalDate start, LocalDate end) {
        return data.values()
                .parallelStream()
                .filter(event -> event.between(start, end))
                .toList();
    }

    public List<Event> findByValue(Double min, Double max) {
        return data.values()
                .parallelStream()
                .filter(event -> event.between(min, max))
                .toList();
    }

}
