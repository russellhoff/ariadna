package es.joninx.ariadna.service;


import es.joninx.ariadna.dto.EventDto;
import es.joninx.ariadna.dto.SourceDto;
import es.joninx.ariadna.factory.EventFactory;
import es.joninx.ariadna.factory.SourceFactory;
import es.joninx.ariadna.io.FileLoader;
import es.joninx.ariadna.model.Event;
import es.joninx.ariadna.model.Source;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 * Main facade class. This class, as most of the rest classes, implements Singleton pattern.
 */
public class Facade {

    private static Facade INSTANCE;
    private final DatabaseService db = DatabaseService.getInstance();

    private Facade(){

    }

    public synchronized static Facade getInstance(){
        if(INSTANCE == null){
            INSTANCE = new Facade();
        }
        return INSTANCE;
    }

    /**
     * Read all files and initialize fake database.
     */
    public void init() throws URISyntaxException, IOException {
        System.out.println(" > Loading initial data...");
        Path sourcesPath = FileLoader.loadResource("sources.csv");
        List<SourceDto> sourcesDto = FileLoader.loadSources(sourcesPath);
        List<Source> sources = sourcesDto.parallelStream()
                .map(SourceFactory::fromDto)
                .toList();
        db.saveSources(sources);

        IntStream.range(1, 7)
            .parallel()
            .forEach(i -> {
                Path eventsPath;
                String resName;
                try {
                    resName = "events_part_" + i + ".csv";
                    eventsPath = FileLoader.loadResource(resName);
                    System.out.println("\t\t + Processing '" + resName + "' file. ");
                    List<EventDto> eventsDtoPart = FileLoader.loadEvents(eventsPath);
                    List<Event> eventsPart = eventsDtoPart.parallelStream()
                        .map(dto -> {
                            Optional<Source> optSource = db.findSourceById(dto.getSourceId());
                            if(optSource.isEmpty())
                                System.err.println("Error looking for source with id " + dto.getSourceId());
                            return optSource.map(source -> EventFactory.fromDto(dto, source)).orElse(null);
                        })
                        .filter(Objects::nonNull)
                        .toList();
                    db.saveEvents(eventsPart);
                } catch (URISyntaxException | IOException e) {
                    throw new RuntimeException(e);
                }

            });
        System.out.println(" > Initial data loaded.");
    }

    public Boolean eventsExist(){
        return db.eventsSize()>0;
    }

    public Boolean sourcesExist(){
        return db.sourcesSize()>0;
    }

    public Optional<Event> findEventById(Integer eventId){
        return db.findEventById(eventId);
    }

    public List<Event> findEventsBySourceName(String sourceName){
        return db.findEventsBySourceName(sourceName);
    }

    public List<Event> findEventsByDate(LocalDate start, LocalDate end){
        return db.findEventsByDate(start, end);
    }

    public List<Event> findEventsByValue(Double min, Double max){
        return db.findEventsByValue(min, max);
    }

}
