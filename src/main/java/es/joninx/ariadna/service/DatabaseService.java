package es.joninx.ariadna.service;

import es.joninx.ariadna.model.Event;
import es.joninx.ariadna.model.Source;
import es.joninx.ariadna.repository.EventsRepository;
import es.joninx.ariadna.repository.SourcesRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Database service class, to invoke and retrieve the desired DB methods.
 */
public class DatabaseService {

    private static DatabaseService INSTANCE;
    private final EventsRepository eventsRepo = EventsRepository.getInstance();
    private final SourcesRepository sourcesRepo = SourcesRepository.getInstance();

    private DatabaseService() {

    }

    public synchronized static DatabaseService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseService();
        }
        return INSTANCE;
    }

    public void saveSources(List<Source> sources) {
        sourcesRepo.insertAll(sources);
    }

    public Optional<Source> findSourceById(Integer sourceId) {
        return sourcesRepo.findOne(sourceId);
    }

    public void saveEvents(List<Event> events) {
        eventsRepo.insertAll(events);
    }

    public Integer eventsSize(){
        return eventsRepo.count();
    }

    public Integer sourcesSize(){
        return sourcesRepo.count();
    }

    public Optional<Event> findEventById(Integer eventId) {
        return eventsRepo.findOne(eventId);
    }

    public List<Event> findEventsBySourceName(String sourceName) {
        return eventsRepo.findBySourceName(sourceName);
    }

    public List<Event> findEventsByDate(LocalDate start, LocalDate end) {
        return eventsRepo.findByDate(start, end);
    }

    public List<Event> findEventsByValue(Double min, Double max) {
        return eventsRepo.findByValue(min, max);
    }
}
