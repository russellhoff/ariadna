package es.joninx.ariadna.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Event DTO class.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class EventDto {
    private Integer id;
    private Integer sourceId;
    private String timestamp;
    private Double value;
}
