package es.joninx.ariadna.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Source DTO class.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SourceDto {
    private Integer id;
    private String name;
}
